# **Blueboard**

Organize your trip with BlueBoard

## **How to run**

We need to have docker installed previously.

```code
yarn start
```

## **Reporting issues**

Found a bug or need a new feature? [Please, open new issue.](https://github.com/taruuuch/blueboard-frontend/issues/new)

## **License**

Copyright (c) 2020 [Artemchuk Dmytro](https://github.com/taruuuch), [Stepanov Valentyn](https://github.com/stenvix)

[The MIT License](https://opensource.org/licenses/MIT)
