import React from 'react';
import { Container } from 'semantic-ui-react';

const App = () => {
    return <Container text>Hello TypeScript</Container>;
};

export default App;
